const sendResponse = (res, results, statusCode, data) => {
  res.status(statusCode).json({
    message: "success",
    results: results,
    data: {
      data: data,
    },
  });
};

module.exports = sendResponse;
