// IMPORTS

const mongoose = require("mongoose");

// DEFINE SCHEMA
const transactionSchema = new mongoose.Schema({
  description: {
    type: String,
    // unique: true,
  },
  amount: {
    type: Number,
  },
  transFromAccId: {
    type: mongoose.Schema.ObjectId,
    ref: "Account",
  },
  transToAccId: {
    type: mongoose.Schema.ObjectId,
    ref: "Account",
  },

  createdAtUtc: { type: Date, default: Date.now },
});

// CREATE TRANSACTION MODEL
const Transaction = mongoose.model("Transaction", transactionSchema);

// EXPORT TRANSACTION
module.exports = Transaction;
