# (Design and development) NodeJS (Express)

### `npm run dev`

Runs the app in the development mode.\
Open [http://localhost:3001](http://localhost:3001) to view it in your browser.

# Database

Mongo Atlas DB database [https://cloud.mongodb.com/v2/65fbdbd132bd79112cef3345#/metrics/replicaSet/65fbdd11cbefd421654d9a32/explorer/bankapp].

# API collection

Postman API collection -
Open [https://drive.google.com/file/d/144FY7xhmvfhzQIoKp_AYajKe6F9bILdE/view?usp=sharing] to view it in your browser.
