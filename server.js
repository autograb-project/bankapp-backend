//IMPORTS
const app = require("./app");
const dotenv = require("dotenv");
const mongoose = require("mongoose");
// import dotenv from 'dotenv';
// dotenv.config({ path: `.env.${process.env.NODE_ENV}` });
//SET .ENV
dotenv.config({
  path: `./config.development.env`,
});
console.log(`./config.${process.env.NODE_ENV}.env`);
//CONNECT DB
const DB = process.env.DATABASE.replace(
  "<PASSWORD>",
  process.env.DATABASE_PASSWORD
);
mongoose
  .connect(DB, {
    // useNewUrlParser: true,
    // useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true, // For Mongoose 5 only. Remove for Mongoose 6+
    serverSelectionTimeoutMS: 1000, // Defaults to 30000 (30 seconds)
  })
  .then(() => {
    console.log("Database Connected- Server", process.env.NODE_ENV);
  })
  .catch((err) => {
    console.log(err);
  });

// START SERVER
const port = process.env.PORT || 3001;
const server = app.listen(port, () => {
  console.log(`app is running on port ${port}...`);
});
