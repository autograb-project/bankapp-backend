const catchAsync = require("../../util/catchAsync");
const AppError = require("../../util/appError");
const Transaction = require("../../models/transactionModel");
const Account = require("../../models/accountModel");
const sendResponse = require("../../util/sendResponse");

exports.createTransaction = catchAsync(async (req, res, next) => {
  const data = req.body;
  const { amount, transFromAccId, transToAccId } = req.body;

  const transaction = await Transaction.create(data);

  const fromAccount = await Account.findOne({ _id: transFromAccId });

  const updateBalanceFromAcc = +fromAccount.balance - +amount;
  const accountFrom = await Account.findByIdAndUpdate(
    transFromAccId,
    { balance: updateBalanceFromAcc },
    {
      new: true,
    }
  );

  const toAccount = await Account.findOne({ _id: transToAccId });

  const updateBalanceToAcc =
    +toAccount.balance === null ? 0 + +amount : +toAccount.balance + +amount;

  const accountTo = await Account.findByIdAndUpdate(
    transToAccId,
    { balance: updateBalanceToAcc },
    {
      new: true,
    }
  );

  sendResponse(res, null, 200, transaction);
});

exports.getAllTransactions = catchAsync(async (req, res, next) => {
  const transactions = await Transaction.find();
  sendResponse(res, transactions.length, 200, transactions);
});
