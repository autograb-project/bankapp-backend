const catchAsync = require("../../util/catchAsync");
const AppError = require("../../util/appError");
const Account = require("../../models/accountModel");
const sendResponse = require("../../util/sendResponse");

exports.createAccount = catchAsync(async (req, res, next) => {
  const data = req.body;

  const account = await Account.create(data);
  sendResponse(res, null, 201, account);
});

exports.getAllAccounts = catchAsync(async (req, res, next) => {
  const accounts = await Account.find();
  sendResponse(res, accounts.length, 200, accounts);
});

exports.updateAccount = catchAsync(async (req, res, next) => {
  const account = await Account.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
  });

  if (!account) {
    return next(new AppError("No account found with that ID", 404));
  }

  sendResponse(res, 1, 204, account);
});
