const express = require("express");
const app = express();
const cors = require("cors");
require("dotenv").config();
var bodyParser = require("body-parser");
const globalErrorHandler = require("./v1/controllers/error/errorController");

app.use(
  cors({
    credentials: true,
    origin: "http://localhost:3000",
  })
); //cross origin

app.use(express.json());
const v1_routes = require("./v1/routers/index");

app.use("/api/v1", v1_routes);

app.use(globalErrorHandler);

module.exports = app;
